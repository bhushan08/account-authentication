from django.urls import path

from .views import (RegisterationAPIView, LoginAPIView, Logout,
                    UserRetriveUpdateAPIView)

app_name = 'authentication'
urlpatterns = [
    path('user/', UserRetriveUpdateAPIView.as_view()),
    path('user/register/', RegisterationAPIView.as_view()),
    path('user/login/', LoginAPIView.as_view()),
    path('user/logout/', Logout.as_view()),
]
