import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

export const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText("#1873E8"),
    backgroundColor: "#1873E8",
    "&:hover": {
      backgroundColor: "#4285f4"
    },
    textTransform: "none"
  }
}))(Button);
