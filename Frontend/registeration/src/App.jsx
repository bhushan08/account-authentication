import React, { Component } from "react";
import "./App.css";
import Login from "./components/login";
import User from "./components/userPage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {
  render() {
    console.log("Login shit rendered");
    return (
      <div className="app-div">
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/user-info" component={User} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
