import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import "../App.css";
import RegisterForm from "./registeration";
import { ColorButton } from "../materialUICustom";
import TelegramIcon from "@material-ui/icons/Telegram";
import { useHistory } from "react-router-dom";
import User from "./userPage";

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#1A73E8"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#DADCE0"
      },
      "&:hover fieldset": {
        borderColor: "#1A73E8"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#1A73E08"
      }
    }
  }
})(TextField);

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      token: "",
      logged_in: localStorage.getItem("token") ? true : false,
      new_user: false
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
    console.log(this.state);
  };

  handleLogin = async e => {
    e.preventDefault();
    await fetch("http://127.0.0.1:8000/api/user/login/", {
      method: "POST",
      body: JSON.stringify({
        user: { email: this.state.email, password: this.state.password }
      }),
      headers: {
        "content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem("token", json.token);

        if (
          localStorage.getItem("token") !== "undefined" &&
          localStorage.getItem("token") !== null
        ) {
          this.setState({
            logged_in: true,
            user: json.username
          });
        } else if (localStorage.getItem("token") === "undefined") {
          localStorage.removeItem("token");
        }
      })
      .catch(err => console.log("This is error catcher", err.res));
    console.log("this is the state of logged_in", this.state.logged_in);
    console.log("TOKEN", localStorage.getItem("token"));

    if (this.state.logged_in) {
      this.props.history.push("/user-info");
    }
  };

  getUserInfo = async () => {
    this.setState(prevState => ({
      new_user: !prevState.new_user
    }));
    // this.setState({
    //   new_user: true
    // });

    try {
      const res = await fetch("http://127.0.0.1:8000/api/user/", {
        method: "GET",
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`
        }
      });
      const data = await res.json();
      this.setState({
        data: data
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  handleLogout = async () => {
    if (this.state.logged_in) {
      localStorage.removeItem("token");
      this.setState({
        logged_in: false,
        username: ""
      });
    }
  };

  notNewUser = async () => {
    this.setState({
      new_user: false
    });
  };

  componentDidMount() {
    if (this.state.logged_in) {
      fetch("http://127.0.0.1:8000/api/user/", {
        method: "GET",
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`
        }
      })
        .then(res => res.json()) //.then(res => console.log(res.json())) for viewing the response from the server
        .catch(err => console.log(err));
    }
    console.log(
      "This is token from localstorage",
      localStorage.getItem("token")
    );
  }

  render() {
    return (
      <div className="login-page-body">
        <div className="form-wrapper-container">
          <div className="left">
            <h1>Something about the company</h1>
          </div>
          <div className="right">
            <div
              className="form-body login-form-body"
              style={{ display: this.state.new_user ? "none" : "block" }}
            >
              <div className="form-header">
                <h2>Log in</h2>
              </div>
              <div className="form-input">
                <div className="row">
                  <CssTextField
                    className="form-input-textfield"
                    id="email"
                    label="Email"
                    variant="outlined"
                    value={this.state.email.value}
                    onChange={this.handleChange}
                  ></CssTextField>
                </div>
                <div className="row">
                  <CssTextField
                    className="form-input-textfield"
                    id="password"
                    label="Password"
                    type="password"
                    variant="outlined"
                    value={this.state.password.value}
                    onChange={this.handleChange}
                  ></CssTextField>
                  <div className="forgot-password">Forgot password?</div>
                </div>
              </div>
              <div className="form-footer">
                <Button
                  className="bottom-left-button"
                  color="primary"
                  texttransform="none"
                  onClick={this.getUserInfo}
                  id="create-button-id"
                  size="large"
                >
                  Create
                </Button>
                {/* <Button
                  className="create-button"
                  color="primary"
                  texttransform="none"
                  onClick={this.handleLogout}
                >
                  Log out
                </Button> */}
                <ColorButton
                  className="bottom-right-button"
                  variant="contained"
                  color="primary"
                  texttransform="none"
                  size="large"
                  disableElevation
                  endIcon={<TelegramIcon />}
                  // className={classes.margin}
                  onClick={this.handleLogin}
                >
                  Log in
                </ColorButton>
              </div>
            </div>
            {this.state.new_user && (
              <RegisterForm
                new_user={this.state.new_user}
                notNewUser={this.notNewUser}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
