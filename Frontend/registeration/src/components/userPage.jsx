import React, { Component } from "react";
import "../userPage.css";
import Button from "@material-ui/core/Button";

class User extends Component {
  constructor(props) {
    super(props);
  }

  handleLogout = async () => {
    localStorage.removeItem("token");
    this.props.history.push("/login");
  };

  componentDidMount() {
    console.log("this is the token", localStorage.getItem("token"));
  }

  render() {
    console.log("this is the token", localStorage.getItem("token"));
    return (
      <div className="user-page-wrapper">
        <div className="username">Name of the user</div>
        <Button
          className="create-button"
          color="primary"
          texttransform="none"
          onClick={this.handleLogout}
        >
          Log out
        </Button>
      </div>
    );
  }
}

export default User;
