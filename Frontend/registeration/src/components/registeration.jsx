import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import SaveIcon from "@material-ui/icons/Save";
import "../App.css";
import { ColorButton } from "../materialUICustom";
import Button from "@material-ui/core/Button";

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#1A73E8"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#DADCE0"
      },
      "&:hover fieldset": {
        borderColor: "#1A73E8"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#1A73E8"
      }
    }
  }
})(TextField);

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      firstName: "",
      lastName: "",
      mobileNumber: "",
      email: "",
      username: "",
      password: "",
      new_user: this.props.new_user
    };
  }

  getVoterData = async () => {
    try {
      const res = await fetch("http://127.0.0.1:8000/voterlist/");
      const data = await res.json();
      this.setState({
        data: data
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    // this.getVoterData();
    if (this.state.new_user) {
      console.log("This is new user as he needs to create a form");
    }
  }

  validate = () => {
    let emailError = "";
    if (/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/.test(this.state.email)) {
    } else {
      emailError = "Invalid email. Please enter a valid email.";
    }

    if (emailError) {
      this.setState({ emailError });
      return false;
    }

    return true;
  };

  handleImageChange = e => {
    this.setState({
      image: e.target.files[0]
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    console.log(this.state);
    fetch("http://127.0.0.1:8000/api/user/register/", {
      method: "POST",
      body: JSON.stringify({
        user: {
          email: this.state.email,
          username: this.state.username,
          password: this.state.password
        }
      }),
      headers: {
        "content-type": "application/json"
      }
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));

    console.log("user created");

    try {
      const res = await fetch("http://127.0.0.1:8000/api/user/", {
        method: "GET",
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`
        }
      });
      const data = await res.json();
      this.setState({
        data: data
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    console.log(
      "this is the new user on registration page",
      this.state.new_user
    );

    return (
      <div
        className="form-body register-form-body"
        style={{ display: this.state.new_user ? "block" : "none" }}
      >
        <div className="form-header">
          <h2>Registeration Form</h2>
        </div>
        <form className="form-input">
          <CssTextField
            className="form-input-textfield"
            id="firstName"
            label="First Name"
            variant="outlined"
            value={this.state.firstName.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="lastName"
            label="Last Name"
            variant="outlined"
            value={this.state.lastName.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="email"
            label="Email ID"
            variant="outlined"
            value={this.state.email.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="username"
            label="Username"
            variant="outlined"
            value={this.state.username.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="password"
            label="Password"
            type="password"
            variant="outlined"
            value={this.state.password.value}
            onChange={this.handleChange}
          ></CssTextField>
        </form>
        <div className="form-footer">
          <Button
            className="bottom-left-button"
            color="primary"
            texttransform="none"
            onClick={this.props.notNewUser}
            id="create-button-id"
          >
            Go Back
          </Button>
          <ColorButton
            id="form-footer-button"
            variant="contained"
            color="primary"
            size="large"
            className="bottom-right-button"
            endIcon={<SaveIcon />}
            onClick={this.handleSubmit}
            disableElevation
          >
            Submit
          </ColorButton>
        </div>
      </div>
    );
  }
}

export default RegisterForm;
